import logo from './logo.svg';
import './App.css';
import DrinksList from './components/drinksList';
import { useState } from 'react';
import FileSelector from './components/fileSelector';

function App() {
  const [drinksData, setDrinksData] = useState();

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {
          drinksData ?
            <DrinksList drinksData={drinksData}></DrinksList> : 
            <FileSelector setDrinksData={setDrinksData}></FileSelector>
        }
      </header>
    </div>
  );
}

export default App;
