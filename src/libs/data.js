export function parseBaseFile(jsonData) {
    return Array.from(jsonData).map((line) => {
        const [name, details, base, packSize, stock = 0] = line;
        return {name, details, base, packSize, stock};
    });
}

export function validateBaseFile(baseFile) {
    if (!Array.isArray(baseFile)) {
        throw new Error('Invalid data structure');
    }

    if (baseFile.length === 0) {
        throw new Error('No data');
    }

    const names = new Set(baseFile.map(line => line.name))
    if (names.size !== baseFile.length) {
        throw new Error('Duplicate names');
    }

    const invalidDataIndex = baseFile.findIndex((drink) => {
        return (typeof drink.base !== 'number') || (typeof drink.packSize !== 'number');
    });
    if (invalidDataIndex !== -1) {
        throw new Error(`Invalid data on row ${invalidDataIndex}`);
    }
}

export function generateFile(drinksData) {
    return JSON.stringify(
        drinksData.map((drink) => {
            const {name, details, base, packSize, stock } = drink;
            return [name, details, base, packSize, stock];
        })
    );
}