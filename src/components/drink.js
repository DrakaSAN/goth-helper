import './drink.css';
import Missing from "./missingDrinks";

function Drink(props) {
    const { packSize, base, stock } = props.drink;

    function setPackSize(packSize) {
        props.setDrinkData({ ...props.drink, packSize })
    }
    
    function setBase(base) {
        props.setDrinkData({ ...props.drink, base })
    }
    
    function setStock(stock) {
        props.setDrinkData({ ...props.drink, stock })
    }

    return <div className="drink">
        <div className="text-info">
            <div>{props.drink.name}</div>
            <div>{props.drink.details}</div>
        </div>
        <label className="base">
        Base: <input name="base" type="number" min={0} defaultValue={base} onChange={e => setBase(Number.parseFloat(e.target.value))}></input> bottles
        </label>

        <label className="pack-size">
        Pack Size: <input name="pack-size" type="number" min={0} defaultValue={packSize} onChange={e => setPackSize(Number.parseFloat(e.target.value))}></input> bottles
        </label>

        <label className="stock">
        Stock: <input name="stock" type="number" min={0} defaultValue={stock} onChange={e => setStock(Number.parseFloat(e.target.value))}></input> bottles
        </label>

        <div> </div>

        <Missing base={base} packSize={packSize} stock={stock}></Missing>
    </div>
}

export default Drink;