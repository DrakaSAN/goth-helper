import { generateFile } from "../libs/data";

export default function Exporter(props) {
    return <input type="button" value="Export data" onClick={() => {
        const data = new Blob([generateFile(props.drinksData)], { type: 'application/json' });
        const link = window.URL.createObjectURL(data);
        const tempLink = document.createElement('a');
        tempLink.href = link;
        tempLink.setAttribute('download', 'base.json');
        tempLink.click();
    }} />
}
