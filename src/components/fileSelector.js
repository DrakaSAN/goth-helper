import { parseBaseFile, validateBaseFile } from "../libs/data";

export default function FileSelector(props) {
    function loadFile(e) {
        const reader = new FileReader();
        reader.onload = (e) => {
            const textData = e.target.result;
            const jsonData = JSON.parse(textData);
            const baseData = parseBaseFile(jsonData);
            validateBaseFile(baseData);
            props.setDrinksData(baseData);
        };
        reader.readAsText(e.target.files[0]);
    }

    return <div>
        Base file: <input type="file" multiple={false} onChange={loadFile}></input>
    </div>
}