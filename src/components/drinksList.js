import { useState } from 'react';
import Drink from './drink';
import Exporter from './exporter';

function DrinksList(props) {
    const [drinksData, setDrinksData] = useState(props.drinksData);
    
    function setDrinkData(i) {
        return (newDrinkData) => {
            const newDrinksData = [...drinksData];
            newDrinksData[i] = newDrinkData;
            setDrinksData(newDrinksData);
        };
    }

    return <div>
        <Exporter drinksData={drinksData} />
        {drinksData.map((drink, i) => <Drink drink={drink} key={drink.name} setDrinkData={setDrinkData(i)} />)}
        <div>
            <label>
                Name: <input type='text' name='Name' id='new_name'/>
            </label>
            <label>
                Details: <input type='text' name='Details' id='new_details'/>
            </label>
            <input
                type={'button'}
                value="Add a line"
                onClick={() => {
                    const name = document.getElementById('new_name').value;
                    const details = document.getElementById('new_details').value;
                    setDrinksData([...drinksData, {
                        name, details, base: 0, packSize: 0, stock: 0
                    }]);
                    document.getElementById('new_name').value = '';
                    document.getElementById('new_details').value = '';
                }}/>
        </div>
    </div>
}

export default DrinksList;