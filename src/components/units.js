const units = [
    'bottles',
    'packs'
];

export default function Units(props) {
    return <select name={props.name} onChange={e => props.onChange(e.target.value)} value={props.value}>
        {units.map((unit) => <option key={unit}>{unit}</option>)}
    </select>
}