function s(number, unit) {
    return `${number} ${unit}${number > 1 ? 's' : ''}`;
}

export default function Missing(props) {
    const {base, stock, packSize} = props
    const rawMissingBottles = (base - stock);
    const missingPacks = Math.floor(rawMissingBottles / packSize);
    const missingBottles = missingPacks * packSize;
    const unit = (props.packSize === 1) ? 'bottle' : 'pack'

    if (rawMissingBottles === 0) {
        return <div className="to-buy">
            Nothing to buy (Already at base)
        </div>
    }

    if (rawMissingBottles <= 0) {
        return <div className="to-buy">
            Nothing to buy (Already have {s(Math.abs(rawMissingBottles), 'bottle')} more than base)
        </div>
    }

    if (missingBottles !== rawMissingBottles) {
        return <div className="to-buy">
            To buy: {s(missingPacks, unit)} for {missingBottles + stock} / {base} bottles or {missingPacks + 1} packs for {missingBottles + stock + packSize} / {base} bottles.
        </div>
    }

    return <div className="to-buy">
        To buy: {s(missingPacks, unit)} for {missingBottles + stock} / {base} bottles.
    </div>
}